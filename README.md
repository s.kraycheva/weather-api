# Weather API

Application using open-weather api and moment library, Vanilla Javascript, HTML and CSS.
Responsive app showing the weather at the moment at the exact location with icons and future forecast for the next several days.

